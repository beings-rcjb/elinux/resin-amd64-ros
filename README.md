# ROS image with pip

Image based on official ROS image with pip and buildpack-deps (manually)
installed.

Pip and setuptools installation derived from [Resin.io library][resin].

`cross-build-start` and `cross-build-end` added as no-ops for compatibility as a
Resin-style image

[resin]: https://github.com/resin-io-library/base-images/blob/master/python/armv7hf/debian/2.7/Dockerfile
